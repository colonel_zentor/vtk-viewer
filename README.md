## Introduction

This is a simple [Qt](http://www.qt.io/)-based viewer for [VTK](http://www.vtk.org/) created in Python.  It is based on the [Embed VTK in PyQT with a QVTKRenderWindowInteractor](http://www.vtk.org/Wiki/VTK/Examples/Python/Widgets/EmbedPyQt2) example from the [VTK Python Examples](http://www.vtk.org/Wiki/VTK/Examples/Python/) page.  The viewer works well with Qt4 Python bindings ([PyQt4](https://pypi.python.org/pypi/PyQt4/4.11.4), and [PySide](https://pypi.python.org/pypi/PySide/1.2.4)) and should also work with Qt5 Python bindings ([PyQt5](https://pypi.python.org/pypi/PyQt5/5.5.1) and [PySide2](https://github.com/PySide/pyside2)).

The primary use case for this viewer is to be used in conjunction with an [Jupyter Notebook](http://jupyter.org/) backed with an [IPython](http://ipython.org/) kernel.  The viewer provides a non-blocking UI for interacting with Python generated VTK actors. Non-blocking implies you can instantiate the view, load an actor and then programmatically modify the actor from the Jupyter Notebook while seeing the real-time effects in the viewer window. Super useful!  

See the example notebook in the notebooks directory for more information.


## Installation

Note, the following instructions are specifically for building and installing vtk-viewer and VTK 7.0 with Qt4 and Python 3 bindings into a [Conda](https://www.continuum.io/downloads) environment. Eventually, there will be a conda package for VTK 7 with Python 3 bindings but since there isn't now, the following instructions will have to suffice. 

In addition, the following instructions are only for building and installing on Linux (specifically Fedora). Windows and Mac users will need to adjust as necessary.  I have not tried to build and install VTK (and/or Qt) on Windows or Mac and have no idea how to go about doing that.  Again, once there is a conda package for VTK 7, that should become trivial for those platforms.

### Conda Environment Configuration

Create a new conda environment based on Python 3, activate it and add the following packages:  `pyqt notebook`


### Building and Installing VTK:

VTK System-level Required Packages:  `ccmake gcc-c++ mesa-libGLU-devel libXmu-devel tbb-devel`

Build VTK and install into the active conda environment:
```
    git clone https://github.com/Kitware/VTK.git
    cd VTK; git checkout -b v7.0.0 tags/v7.0.0
    mkdir build-v7.0.0; cd build-v7.0.0
    cmake \
        -DCMAKE_INSTALL_PREFIX:PATH=${CONDA_ENV_PATH} \
        -DVTK_WRAP_PYTHON:BOOL=ON \
        -DVTK_PYTHON_VERSION:STRING=3 \
        -DVTK_Group_Qt:BOOL=ON \
        -DVTK_QT_VERSION:STRING=4 \
        -DQT_QMAKE_EXECUTABLE:PATH=${CONDA_ENV_PATH}/bin/qmake \
        -DCMAKE_PREFIX_PATH:PATH=/usr/lib64/cmake \
        -DBUILD_SHARED_LIBS:BOOL=ON \
        -DCMAKE_BUILD_TYPE:STRING=RELEASE \
        -DBUILD_TESTING:BOOL=OFF \
        -DBUILD_DOCUMENTATION:BOOL=OFF \
        -DVTK_SMP_IMPLEMENTATION_TYPE:STRING=TBB \
        -DVTK_RENDERING_BACKEND:STRING=OpenGL2 \
        .. 
    make -j$(nproc)
    make install
```

To install VTK into another conda environment:
```
    cmake \
        -DCMAKE_INSTALL_PREFIX:PATH=${CONDA_ENV_PATH} \
        ..
    make install 
```

### Installing the VTK viewer

To install vtk-viewer into the active conda environment, simply execute:

```
    pip install git+https://gitlab.com/colonel_zentor/vtk-viewer.git
```

Or if you want the source on your computer for additional hacking:

```
    git clone https://gitlab.com/colonel_zentor/vtk-viewer.git
    cd vtk-viewer
    python setup.py develop
```

------
Copyright &copy; 2016 Jack Zentner (jack.zentner@gmail.com)