# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------
#  Copyright (c) 2016, Jack Zentner (jack.zentner@gmail.com)
#
#  Distributed under the terms of the MIT License.
#
#  The full license is in the file LICENSE.txt, distributed with this software.
#-----------------------------------------------------------------------------

__version_info__ = (0, 1, 0)
__version__ = '.'.join(map(str, __version_info__))