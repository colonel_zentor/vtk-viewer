#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------
#  Copyright (c) 2016, Jack Zentner (jack.zentner@gmail.com)
#
#  Distributed under the terms of the GPLv2 License.
#
#  The full license is in the file LICENSE.txt, distributed with this software.
#-----------------------------------------------------------------------------

from setuptools import find_packages
from distutils.core import setup

__version__ = 'undefined'
exec(open("version.py").read())


setup(
    name='vtk-viewer',
    version=__version__,
    description='A non-blocking, Qt-based, UI for viewing VTK actors with the Jupyter Notebook.',
    long_description=""" """,
    author='Jack Zentner',
    author_email='jack.zentner@gmail.com',
    url='https://gitlab.com/colonel_zentor/vtk-viewer',
    license='MIT',
    packages=find_packages(exclude=('docs', 'setup', 'tests')),
)